#include <iostream>
#include <vector>
#include <algorithm>
#include "../../include/easy_print.hpp"

/*
 * Given a list of sorted numbers (can both negative and positive), return the list of
 * numbers squared in sorted order.
 *
 * Sort this in O(n) time
 */


using namespace std;

class Solution
{
public:
  vector<long> sort_squared(vector<int> &numbers)
  {
    vector<long> result(numbers.size());
    size_t left = 0;
    size_t right = numbers.size();

    for (size_t i = numbers.size(); i > 0; --i) {
      if (numbers[left] * numbers[left] < numbers[right - 1] * numbers[right - 1]) {
        result[i - 1] = numbers[right - 1] * numbers[right - 1];
        --right;
      }
      else {
        result[i - 1] = numbers[left] * numbers[left];
        ++left;
      }
    }

    return result;
  }
};

int main()
{
  vector<pair<vector<int>, vector<long>>> tests = {
                                                   {{}, {}},
                                                   {{-2, -1, -1, 0}, {0, 1, 1, 4}},
                                                   {{-5, -2, -1, 0, 2, 4}, {0, 1, 4, 4, 16, 25}},
                                                   {{0, 2, 4}, {0, 4, 16}},
  };

  Solution sol;

  for (auto &test: tests) {
    vector<long> &expected = test.second;
    vector<long> result = sol.sort_squared(test.first);

    if (expected != result)
      cout << "FAIL expected:" << expected << " result:" << result << endl;
    else
      cout << "PASS" << endl;
  }
  return 0;
}
