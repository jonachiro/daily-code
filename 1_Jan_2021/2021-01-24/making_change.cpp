/*
 * Given a list of possible coins in cents, and an amount (in cents) n,
   return the minimum number of coins needed to create the amount n.

   If it is not possible to create the amount using the given coin denomination, return None.

   print(make_change([1, 5, 10, 25], 36))
   # 3 coins (25 + 10 + 1)
 */
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
  const int NON_VISITED = -1;
  int dp_func(vector<int> &dp, vector<int> &coins, int amount)
  {
    if (amount < 0)
      return numeric_limits<int>::max();

    if (amount == 0)
      dp[amount] = 0;

    if (dp[amount] != NON_VISITED)
      return dp[amount];

    int min_coins = numeric_limits<int>::max();
    for (int coin: coins) {
      int current_coins = dp_func(dp, coins, amount - coin);
      min_coins = min(current_coins, min_coins);
    }

    if (min_coins == numeric_limits<int>::max())
      dp[amount] = numeric_limits<int>::max();
    else
      dp[amount] = min_coins + 1;

    return dp[amount];
  }

public:
  int make_change(vector<int> &coins, int amount)
  {
    vector<int> dp(amount + 1, NON_VISITED);
    dp_func(dp, coins, amount);

    int change = dp[amount];
    return change;
  }
};

int main()
{
  vector<pair<pair<vector<int>, int>, int>> tests = {
                                                     {{{1,2,3},5}, 2},
                                                     {{{1,2,3},6}, 2},
                                                     {{{1,2,3},9}, 3},
                                                     {{{1,2,3},1}, 1},
                                                     {{{1,2,3},2}, 1},
                                                     {{{1,2,3},3}, 1},
                                                     {{{1,2,3},4}, 2},
  };

  Solution sol;
  for (auto &test: tests) {
    vector<int> coins = test.first.first;
    int amount = test.first.second;
    int expected = test.second;

    int result = sol.make_change(coins, amount);

    if (result != expected)
      cout << "FAIL expected:" << expected << " result:" << result << endl;
    else
      cout << "PASS" << endl;
  }

  return 0;
}
