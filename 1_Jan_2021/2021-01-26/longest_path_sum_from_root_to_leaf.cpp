#include <iostream>
#include <vector>
#include <algorithm>
// Given a binary tree, find and return the largest path from root to leaf.

using namespace std;

template <typename T>
ostream &operator<<(ostream &os, vector<T> &v)
{
  os << "[";
  if (v.size()) {
    for (size_t i = 0; i < v.size() - 1; ++i)
      os << v[i] << ",";
    os << v[v.size() - 1];
  }
  os << "]";
  return os;
}

class Node
{
public:
  Node *left, *right;
  int val;
  Node(int val, Node *l=nullptr, Node *r=nullptr): left{l}, right{r}, val{val} {}
};

class Solution
{
private:
  int max_sum;
  vector<int> m_largest_path;

public:
  void get_largest_path(int current_sum, vector<int> &current_path, Node *node)
  {
    if (!node)
      return;

    // Check if this is a leaf
    if (!node->left && !node->right) {
      if (current_sum + node->val > max_sum) {
        m_largest_path = current_path;
        m_largest_path.push_back(node->val);
        max_sum = current_sum + node->val;
      }
      return;
    }

    current_path.push_back(node->val);
    if (node->left) {
      get_largest_path(current_sum + node->val, current_path, node->left);
    }
    if (node->right) {
      get_largest_path(current_sum + node->val, current_path, node->right);
    }
    current_path.pop_back();
  }

vector<int> largest_path(Node *node)
{
  vector<int> current_path;
  max_sum = numeric_limits<int>::min();
  get_largest_path(0, current_path, node);
  return m_largest_path;
}

};

int main()
{
  Node root(1, new Node(2, new Node (5, new Node (5))), new Node(3));

  Solution sol;
  vector<int> longest_path = sol.largest_path(&root);
  cout << "longest_path: " << longest_path << endl;

  return 0;
}
