// Given a list of numbers and an integer k, partition/sort the list such that the all numbers
// less than k occur before k, and all numbers greater than k occur after the number k.

// def partition_list(nums, k):
// # Fill this in.
// print(partition_list([2, 2, 2, 5, 2, 2, 2, 2, 5], 3))
//                          # [2, 2, 2, 2, 2, 2, 2, 2, 5]

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

typedef pair<vector<int>,int> input_t;
typedef vector<int> output_t;

template <typename T>
bool operator==(vector<T> &lhs, vector<T> &rhs)
{
  if (lhs.size() != rhs.size())
    return false;

  for (size_t i = 0; i < lhs.size(); ++i) {
    if (lhs[i] != rhs[i])
      return false;
  }

  return true;
}

template <typename T>
ostream &operator<<(ostream &os, vector<T> &v)
{
  os << "[";
  if (v.size()) {
    for (size_t i = 0; i < v.size() - 1; ++i)
      os << v[i] << ",";
    os << v[v.size() - 1];
  }
  os << "]";
  return os;
}

class Solution
{
public:
  void partition_list(vector<int> &nums, int k)
  {
    size_t lower = 0;
    size_t greater = nums.size();
    size_t current = 0;
    while (current < greater) {
      if (nums[current] < k) {
        swap(nums[current], nums[lower]);
        ++lower;
        ++current;
      }
      else if (nums[current] > k){
        --greater;
        swap(nums[current], nums[greater]);
      }
      else {
        ++current;
      }
    }
  }
};

int main()
{
  vector<pair<input_t, vector<int>>> tests = {
                                              {{{1}, 3},{1}},
                                              {{{5}, 3},{5}},
                                              {{{1,2,3,4,4}, 3},{1,2,3,4,4}},
                                              {{{2,5,2,2,5}, 3},{2,2,2,5,5}},
                                              {{{2,5,5,2,5}, 3},{2,2,5,5,5}},
                                              {{{1,1,3,2,3}, 3},{1,1,2,3,3}},
  };

  Solution sol;
  for (auto &test: tests) {
    vector<int> input = test.first.first;
    vector<int> result = test.first.first;
    int k = test.first.second;
    output_t &expected = test.second;
    sol.partition_list(result, k);

    if (result == expected)
      cout << "PASS" << endl;
    else
      cout << "FAIL input:" << input << " expected:" << expected << " result:" << result << endl;
  }


  return 0;
}
