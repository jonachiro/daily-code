INCLUDE_PATH := .$(CURDIR)/include/

build-dirs_ := $(dir $(wildcard */))
build-dirs := $(filter-out include/, $(build-dirs_))

all: $(build-dirs)
	@echo $(fo)

$(build-dirs):
	@echo doing $@
	make -C $@

.PHONY: all $(build-dirs)
