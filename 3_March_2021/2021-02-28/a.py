"""
Create a function for converting integer into hex representation
"""
BITS_TO_HEX_MAP = {
                    1: "1", 2: "2", 3: "3", 4: "4", 5: "5",
                    6: "6", 7: "7", 8: "8", 9: "9", 10: "A",
                    11: "B", 12: "C", 13: "D", 14: "E", 15: "F"}


def to_hex(number):
    """ Return a hex-string representation of number """
    if number == 0:
        return "0x0"

    result = "0x"
    while number:
        digit = number & 0xF
        result += BITS_TO_HEX_MAP[digit]
        number >>= 4

    return result


print(to_hex(0x22))
print(to_hex(0x0))
print(to_hex(0x1234))
print(to_hex(0xAAAA))
print(to_hex(15))
