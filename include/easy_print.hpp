#include <iostream>
#include <vector>
#include <algorithm>

template <typename T>
std::ostream &operator<<(std::ostream &os, std::vector<T> &v)
{
    os << "[";
    if (v.size()) {
        for (size_t i = 0; i < v.size() - 1; ++i)
            os << v[i] << ",";
        os << v[v.size() - 1];
    }
    os << "]";
    return os;
}

template <typename T>
bool operator==(std::vector<T> &lhs, std::vector<T> &rhs)
{
    if (lhs.size() != rhs.size())
        return false;

    for (size_t i = 0; i < lhs.size(); ++i) {
        if (lhs[i] != rhs[i])
            return false;
    }

    return true;
}
